import burger from "./img/burger.png";
import pizza from "./img/pizza.png";
import seafood from "./img/seafood.png";

export { burger, pizza, seafood };
