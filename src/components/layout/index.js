import React from "react";
import { Container } from "react-bootstrap";
import styled from "styled-components";

const Layout = (props) => {
	
	return (
		<Content>
			<Container fluid="md">{props.children}</Container>
		</Content>
	);
};
export default Layout;

const Content = styled.div`
	background-color: #222;
	height: 100vh;

	@media (max-width: 978px) {
		height: 150vh;
	}
	@media (max-width: 649px) {
		height: 200vh;
	}
`;
