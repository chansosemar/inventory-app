import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button } from "react-bootstrap";

const ListProduk = () => {
	const dispatch = useDispatch();
	const listData = useSelector((state) => state.product.data);
	const deleteProduct = (id) => {
		dispatch({ type: "delProduct", payload: id });
	};
	return (
		<div style={styles.container}>
			{listData
				? listData.map((item, index) => (
						<div style={styles.content} key={index}>
							<img
								src={item.foto_produk}
								alt="foto produk"
								style={styles.img}
							/>
							<h5 style={styles.txt}>{item.nama_produk}</h5>
							<p style={styles.txt}>{item.nama_kategori}</p>
							<p style={styles.txt}>
								Stock : {item.jumlah_barang}
							</p>
							<div style={styles.btn}>
								<Button variant="primary" size="sm">
									Update
								</Button>{" "}
								<Button
									onClick={() =>
										deleteProduct(item.id_produk)
									}
									variant="danger"
									size="sm"
								>
									Delete
								</Button>
							</div>
						</div>
				  ))
				: null}
		</div>
	);
};

export default ListProduk;

const styles = {
	txt: {
		color: "#fff",
		fontFamily: "Montserrat",
	},
	content: {
		backgroundColor: "#313131",
		width: "100%",
		padding: 20,
	},
	container: {
		display: "grid",
		gridTemplateColumns: "repeat(auto-fill, 200px)",
		gap: 20,
		marginTop: 20,
	},
	img: {
		width: "100%",
		marginBottom: 10,
	},
	btn: {
		display: "flex",
		justifyContent: "space-around",
	},
};
