import React from "react";
import { Navbar } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

const Navigation = () => {
	const name = useSelector((state) => state.user.data[0]);
	const dispatch = useDispatch();
	console.log(name)
	return (
		<Navbar>
			<Navbar.Brand style={styles.navbar}>
				<Link style={styles.logo} to="/">
					INVENTORY APP
				</Link>
			</Navbar.Brand>
			<Navbar.Toggle />
			<Navbar.Collapse className="justify-content-end">
				<Link style={styles.download} to="/download">
					Download Report
				</Link>
				<Navbar.Text style={styles.navbar}>Hello, {name ? name.nama_user : null} </Navbar.Text>

				<Link
					style={styles.logout}
					onClick={() => dispatch({ type: "logout" })}
					to="/"
				>
					Logout
				</Link>
			</Navbar.Collapse>
		</Navbar>
	);
};

export default Navigation;

const styles = {
	navbar: {
		color: "#fff",
		fontFamily: "Montserrat",
	},
	logo: {
		color: "#fff",
		fontFamily: "Montserrat",
		textDecoration: "none",
	},
	logout: {
		color: "#d72222",
		fontFamily: "Montserrat",
		margin: 10,
	},
	download: {
		fontFamily: "Montserrat",
		margin: 10,
	},
};
