import React from "react";
import {
	Navbar,
	Nav,
	Form,
	FormControl,
	Button,
	NavDropdown,
} from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

const Menu = () => {
	const listCategories = useSelector((state) => state.categories.data);

	const listSort = [
		{
			id: 1,
			title: "Newest",
		},
		{
			id: 2,
			title: "Oldest",
		},
		{
			id: 3,
			title: "A - Z",
		},
		{
			id: 4,
			title: "Z - A",
		},
	];

	return (
		<Navbar style={styles.menu} expand="lg">
			<Navbar.Toggle
				aria-controls="basic-navbar-nav"
				style={{ backgroundColor: "#fff", width: "40%" }}
			/>
			<Navbar.Collapse
				id="basic-navbar-nav"
				className="justify-content-between"
			>
				<NavDropdown title="All Categories" id="basic-nav-dropdown">
					{listCategories.map((item, index) => (
						<NavDropdown.Item key={index}>
							{item.nama_kategori}
						</NavDropdown.Item>
					))}
				</NavDropdown>
				<NavDropdown title="Sort by" id="basic-nav-dropdown">
					{listSort.map((item, index) => (
						<NavDropdown.Item key={index}>
							{item.title}
						</NavDropdown.Item>
					))}
				</NavDropdown>
				<Nav.Link style={styles.menu}>+ Add Product</Nav.Link>
			</Navbar.Collapse>
			<Form inline>
				<FormControl
					style={styles.formInput}
					type="text"
					placeholder="Search Product"
					className=" mr-sm-2"
				/>
				<Button style={styles.search}>Search</Button>
			</Form>
		</Navbar>
	);
};

export default Menu;

const styles = {
	menu: {
		color: "#53C9C2",
		fontFamily: "Montserrat",
	},
	search: {
		backgroundColor: "#53C9C2",
		borderWidth: 0,
		borderRadius: 20,
		color: "#222",
	},
	formInput: {
		backgroundColor: "#222",
		borderColor: "#fff",
		borderRadius: 20,
		color: "#fff",
	},
};
