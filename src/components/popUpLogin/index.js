import React, { useState } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { render } from "react-dom";
import { Dots } from "react-activity";
import 'react-activity/lib/Dots/Dots.css';

const PopUpLogin = (props) => {
	const [username, setUsername] = useState("chansosemar");
	const [password, setPassword] = useState("12345");
	const isLoading = useSelector((state) => state.auth.isLoading);
	const dispatch = useDispatch();
	const submit = () => {
		const data = {
			nama_user: username,
			password: password,
		};
		dispatch({ type: "login", payload: data });
	};

	return (
		<Modal
			{...props}
			size="lg"
			aria-labelledby="contained-modal-title-vcenter"
			centered
		>
			<Modal.Body style={styles.modal}>
				<Form>
					<Form.Group controlId="formBasicEmail">
						<Form.Label>Username</Form.Label>
						<Form.Control
							onChange={(e) => setUsername(e.target.value)}
							style={styles.formInput}
							type="username"
							placeholder="Username"
						/>
					</Form.Group>
					<Form.Group controlId="formBasicPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control
							onChange={(e) => setPassword(e.target.value)}
							style={styles.formInput}
							type="password"
							placeholder="Password"
						/>
					</Form.Group>
					<div className="text-center">
						{!isLoading ? (
							<Button
								onClick={() => submit()}
								style={styles.submit}
							>
								Submit
							</Button>
						) : (
							<Dots />
						)}

						<p style={{ marginTop: 20 }}>Don't have an account ?</p>
						<Button onClick={() => submit()} style={styles.submit}>
							Register
						</Button>
					</div>
				</Form>
			</Modal.Body>
		</Modal>
	);
};

export default PopUpLogin;

const styles = {
	modal: {
		fontFamily: "Montserrat",
		backgroundColor: "#222",
		color: "#fff",
	},
	submit: {
		backgroundColor: "#53C9C2",
		borderWidth: 0,
		color: "#222",
		fontWeight: 600,
		width: "100%",
	},
	formInput: {
		backgroundColor: "#222",
		borderColor: "#fff",
		borderRadius: 20,
		color: "#fff",
	},
};
