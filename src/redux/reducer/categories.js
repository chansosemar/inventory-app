const initialState = {
	isLoading:false,
	data:[]
};

const categories = (state = initialState, action) => {
	switch (action.type) {
		case 'getCategories': {
			return {
				...state,
				isLoading:true
			};
		}
		case 'getCategoriesSuccess': {
			return {
				...state,
				isLoading:false,
				data: action.payload
			};
		}
		case 'getCategoriesFailed': {
			return {
				...state,
				isLoading:false
			};
		}
		default:
			return state;
	}
};

export default categories;
