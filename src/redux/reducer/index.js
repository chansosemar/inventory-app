import { combineReducers } from "redux";
import auth from "./auth";
import user from "./user";
import product from "./product";
import categories from "./categories";

export default combineReducers({
	auth,
	user,
	product,
	categories,
});
