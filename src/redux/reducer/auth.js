const initialState = {
	isLoading:false,
	isLoggedIn: false,
};

const auth = (state = initialState, action) => {
	switch (action.type) {
		case 'login': {
			return {
				...state,
				isLoading:true
			};
		}
		case 'loginSuccess': {
			return {
				...state,
				isLoading:false,
				isLoggedIn: true,
			};
		}
		case 'loginFailed': {
			return {
				...state,
				isLoading:false
			};
		}
		case 'logout': {
			return {
				...state,
				isLoggedIn: false,
			};
		}
		default:
			return state;
	}
};

export default auth;
