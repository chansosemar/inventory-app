const initialState = {
	isLoading:false,
	data:[]
};

const user = (state = initialState, action) => {
	switch (action.type) {
		case 'getUser': {
			return {
				...state,
				isLoading:true
			};
		}
		case 'getUserSuccess': {
			return {
				...state,
				isLoading:false,
				data: action.payload
			};
		}
		case 'getUserFailed': {
			return {
				...state,
				isLoading:false
			};
		}
		default:
			return state;
	}
};

export default user;
