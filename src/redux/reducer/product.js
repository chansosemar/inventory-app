const initialState = {
	isLoading:false,
	data:[]
};

const product = (state = initialState, action) => {
	switch (action.type) {
		case 'getProduct': {
			return {
				...state,
				isLoading:true
			};
		}
		case 'getProductSuccess': {
			return {
				...state,
				isLoading:false,
				data: action.payload
			};
		}
		case 'getProductFailed': {
			return {
				...state,
				isLoading:false
			};
		}
		case 'delProduct': {
			return {
				...state,
				isLoading:true
			};
		}
		case 'delProductSuccess': {
			return {
				...state,
				isLoading:false,
			};
		}
		case 'delProductFailed': {
			return {
				...state,
				isLoading:false
			};
		}
		default:
			return state;
	}
};

export default product;
