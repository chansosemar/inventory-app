import { apiLogin } from "../mainApi";
import { takeLatest, put } from "redux-saga/effects";
import { saveToken, saveAccountId } from "../asyncStorage";

function* login(action) {
	try {
		const resLogin = yield apiLogin(action.payload);

		if (resLogin && resLogin.data) {
			yield saveToken(resLogin.data.token);
			yield saveAccountId(resLogin.data.id_user);
			yield put({ type: "loginSuccess" });
			yield put({ type: "getUser" });
			yield put({ type: "getProduct" });
			yield put({ type: "getCategories" });
		} else {
			console.log("login gagal");
			yield put({ type: "loginFailed" });
		}
	} catch (e) {
		console.log("login gagal", e);
		yield put({ type: "loginFailed" });
	}
}

function* auth() {
	yield takeLatest("login", login);
}

export default auth;
