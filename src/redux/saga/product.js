import { apiGetProduct, apiDelProduct } from "../mainApi";
import { takeLatest, put } from "redux-saga/effects";
import { getHeaders } from "../asyncStorage";

function* getProduct() {
	try {
		const headers = yield getHeaders();
		const resGetProduct = yield apiGetProduct(headers);
		if (resGetProduct && resGetProduct.data) {
			yield put({
				type: "getProductSuccess",
				payload: resGetProduct.data,
			});
		} else {
			console.log("get product gagal");
			yield put({ type: "getProductFailed" });
		}
	} catch (e) {
		console.log("get product gagal", e);
		yield put({ type: "getProductFailed" });
	}
}

function* delProduct(action) {
	try {
		const headers = yield getHeaders();
		console.log('1')
		const resDelProduct = yield apiDelProduct(action.payload, headers);
		console.log('2', resDelProduct)
		if (resDelProduct) {
			yield put({
				type: "delProductSuccess",
			});
			console.log('3')
			yield put({ type: "getProduct" });
			console.log('4')
		} else {
			console.log("del product gagal");
			yield put({ type: "delProductFailed" });
		}
	} catch (e) {
		console.log("del product gagal", e);
		yield put({ type: "delProductFailed" });
	}
}

function* product() {
	yield takeLatest("delProduct", delProduct);
	yield takeLatest("getProduct", getProduct);
}

export default product;
