import { apiGetCategories } from "../mainApi";
import { takeLatest, put } from "redux-saga/effects";
import { getHeaders } from "../asyncStorage";

function* getCategories() {
	try {
		const headers = yield getHeaders();
		console.log('1', headers)
		const resGetCategories = yield apiGetCategories(headers);
		console.log('2', resGetCategories)
		if (resGetCategories && resGetCategories.data) {
			console.log("3", resGetCategories.data);

			yield put({ type: "getCategoriesSuccess", payload: resGetCategories.data });
		} else {
			console.log("get categories gagal");
			yield put({ type: "getCategoriesFailed" });
		}
	} catch (e) {
		console.log("get categories gagal", e);
		yield put({ type: "getCategoriesFailed" });
	}
}

function* categories() {
	yield takeLatest("getCategories", getCategories);
}

export default categories;
