import { all } from "redux-saga/effects";
import auth from "./auth";
import user from "./user";
import product from "./product";
import categories from "./categories";

export default function* rootSaga() {
	yield all([auth(), user(), product(), categories()]);
}
