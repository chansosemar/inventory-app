import { apiGetUser } from "../mainApi";
import { takeLatest, put } from "redux-saga/effects";
import { getHeaders, getAccountId } from "../asyncStorage";

function* getUser() {
	try {
		const headers = yield getHeaders();

		const accountId = yield getAccountId();

		const resGetUser = yield apiGetUser(accountId, headers);

		if (resGetUser && resGetUser.data) {
			yield put({ type: "getUserSuccess", payload: resGetUser.data });
		} else {
			console.log("get user gagal");
			yield put({ type: "getUserFailed" });
		}
	} catch (e) {
		console.log("get user gagal", e);
		yield put({ type: "getUserFailed" });
	}
}

function* user() {
	yield takeLatest("getUser", getUser);
}

export default user;
