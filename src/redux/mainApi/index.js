import axios from "axios";

const baseUrl = "https://chanso-psqldb.herokuapp.com";

// AUTH
export function apiLogin(dataLogin) {
	return axios({
		method: "POST",
		url: baseUrl + "/login",
		data: dataLogin,
	});
}
// USER
export function apiGetUser(id, headers) {
	return axios({
		method: "GET",
		url: baseUrl + "/user/" + id,
		headers,
	});
}

// PRODUCT
export function apiGetProduct(headers) {
	return axios({
		method: "GET",
		url: baseUrl + "/product",
		headers,
	});
}

export function apiDelProduct(id,headers) {
	return axios({
		method: "DELETE",
		url: baseUrl + "/product/" + id,
		headers,
	});
}

// CATEGORIES
export function apiGetCategories(headers) {
	return axios({
		method: "GET",
		url: baseUrl + "/categories",
		headers,
	});
}
