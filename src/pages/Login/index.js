import React, { useState } from "react";
import styled from "styled-components";
import { PopUpLogin } from "../../components";

const Login = () => {
	const [modalSHow, setModalSHow] = useState(false);

	return (
		<Content>
			<div>
				<Text>WELCOME TO</Text>
				<Text>INVENTORY APP</Text>
				<TextBtn onClick={() => setModalSHow(true)}>LOGIN</TextBtn>
				<PopUpLogin
					show={modalSHow}
					onHide={() => setModalSHow(false)}
				/>
			</div>
		</Content>
	);
};

export default Login;

const Content = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100vh;
	color: #fff;
	font-family: "Montserrat", sans-serif;
`;

const Text = styled.h1`
	font-weight: 100;
	margin: 0;
`;

const TextBtn = styled.h1`
	text-align: right;
	font-weight: 600;

	&:hover {
		color: #53c9c2;
		cursor: pointer;
	}
`;
