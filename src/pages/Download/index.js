import React from "react";
import { Navbar, Menu } from "../../components";

const Download = () => {
	return (
		<div>
			<Navbar />
			<Menu />
			<h1 style={{color:'#fff'}}>Download</h1>
		</div>
		)
};

export default Download;
