import React from "react";
import { Navbar, Menu, ListProduk } from "../../components";
// import { Table } from "react-bootstrap";

const Home = () => {
	return (
		<div>
			<Navbar />
			<Menu />
			<ListProduk/>
		</div>
	);
};

export default Home;

// const styles = {
// 	text: {
// 		color: "#fff",
// 		marginTop:'2%'
// 	},
// };
