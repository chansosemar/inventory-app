import React from "react";
import ReactDOM from "react-dom";
import MainNavigator from "./MainNavigator";
import { Provider } from "react-redux";
import store from "./redux/store";

const App = () => {
	return (
		<Provider store={store}>
			<MainNavigator />
		</Provider>
	);
};

ReactDOM.render(<App />, document.getElementById("root"));
