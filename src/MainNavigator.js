import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Login, Home, Download } from "./pages";
import { Layout } from "./components";
import "bootstrap/dist/css/bootstrap.min.css";
import { useSelector } from "react-redux";

function MainNavigator() {
  const auth = useSelector((state) => state.auth.isLoggedIn);

  return (
    <React.Fragment>
      <Layout>
        <Router>
          <Switch>
            {auth ? (
              <>
                <Route exact path="/" component={Home} />
                <Route exact path="/download" component={Download} />
              </>
            ) : (
              <>
                <Route exact path="/" component={Login} />
              </>
            )}
          </Switch>
        </Router>
      </Layout>
    </React.Fragment>
  );
}

export default MainNavigator;
